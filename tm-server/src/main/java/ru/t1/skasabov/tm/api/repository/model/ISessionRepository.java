package ru.t1.skasabov.tm.api.repository.model;

import ru.t1.skasabov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}

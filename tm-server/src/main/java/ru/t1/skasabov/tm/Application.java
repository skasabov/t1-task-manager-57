package ru.t1.skasabov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.skasabov.tm.component.Bootstrap;
import ru.t1.skasabov.tm.configuration.ServerConfiguration;

public final class Application {

    public static void main(@Nullable final String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }

}

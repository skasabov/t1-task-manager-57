package ru.t1.skasabov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.skasabov.tm.api.repository.model.IUserRepository;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.api.service.model.IUserService;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.UserNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.exception.user.ExistsEmailException;
import ru.t1.skasabov.tm.exception.user.ExistsLoginException;
import ru.t1.skasabov.tm.exception.user.PasswordHashEmptyException;
import ru.t1.skasabov.tm.exception.field.RoleEmptyException;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    public IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<User> collection) {
        if (collection == null) return;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAll(collection);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<User> addAll(@Nullable final Collection<User> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<User> set(@Nullable final Collection<User> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.set(models);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    public User add(@Nullable final User model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull List<User> users;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            users = repository.findAll();
        }
        finally {
            entityManager.close();
        }
        return users;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            user = repository.findOneById(id);
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            user = repository.findOneByIndex(index);
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User removeOne(@Nullable final User model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOne(model);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User model = findOneById(id);
        if (model == null) return null;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final User model = findOneByIndex(index);
        if (model == null) return null;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeOneByIndex(index);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        long size;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            size = repository.getSize();
        }
        finally {
            entityManager.close();
        }
        return size;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsUser;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            existsUser = repository.existsById(id);
        }
        finally {
            entityManager.close();
        }
        return existsUser;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordHashEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordHashEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordHashEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            user = repository.findByLogin(login);
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User user;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            user = repository.findByEmail(email);
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return removeOne(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return removeOne(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        boolean existsUser;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            existsUser = repository.isLoginExist(login);
        }
        finally {
            entityManager.close();
        }
        return existsUser;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        boolean existsUser;
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            existsUser = repository.isEmailExist(email);
        }
        finally {
            entityManager.close();
        }
        return existsUser;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final IUserRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

}

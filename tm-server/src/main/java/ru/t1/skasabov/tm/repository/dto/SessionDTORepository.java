package ru.t1.skasabov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.skasabov.tm.dto.model.SessionDTO;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        return entityManager.createQuery("SELECT s FROM SessionDTO s", SessionDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.id = :id", SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public SessionDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT s FROM SessionDTO s", SessionDTO.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(s) FROM SessionDTO s", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    public void removeAll() {
        @NotNull final List<SessionDTO> sessions = findAll();
        for (@NotNull final SessionDTO session : sessions) {
            entityManager.remove(session);
        }
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId AND s.id = :id",
                        SessionDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public SessionDTO findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(s) FROM SessionDTO s WHERE s.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndex(userId, index));
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<SessionDTO> sessions = findAll(userId);
        for (@NotNull final SessionDTO session : sessions) {
            entityManager.remove(session);
        }
    }
    
}

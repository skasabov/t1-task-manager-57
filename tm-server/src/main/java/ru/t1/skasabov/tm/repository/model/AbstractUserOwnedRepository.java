package ru.t1.skasabov.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.skasabov.tm.model.AbstractUserOwnedModel;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

}

package ru.t1.skasabov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.SessionDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return entityManager.createQuery("SELECT u FROM UserDTO u", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT u FROM UserDTO u WHERE u.id = :id", UserDTO.class)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT u FROM UserDTO u", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(u) FROM UserDTO u", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOne(@NotNull final UserDTO model) {
        @NotNull final List<ProjectDTO> projects = entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", model.getId())
                .getResultList();
        @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setParameter("userId", model.getId())
                .getResultList();
        @NotNull final List<SessionDTO> sessions = entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId", SessionDTO.class)
                .setParameter("userId", model.getId())
                .getResultList();
        for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
        for (@NotNull final SessionDTO session : sessions) entityManager.remove(session);
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final List<TaskDTO> projectTasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                    .setParameter("projectId", project.getId())
                    .getResultList();
            for (@NotNull final TaskDTO projectTask : projectTasks) entityManager.remove(projectTask);
            entityManager.remove(project);
        }
        entityManager.remove(model);
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) return;
        @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setParameter("userId", user.getId())
                .getResultList();
        @NotNull final List<SessionDTO> sessions = entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId", SessionDTO.class)
                .setParameter("userId", user.getId())
                .getResultList();
        @NotNull final List<ProjectDTO> projects = entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", user.getId())
                .getResultList();
        for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
        for (@NotNull final SessionDTO session : sessions) entityManager.remove(session);
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final List<TaskDTO> projectTasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                    .setParameter("projectId", project.getId())
                    .getResultList();
            for (@NotNull final TaskDTO projectTask : projectTasks) entityManager.remove(projectTask);
            entityManager.remove(project);
        }
        entityManager.remove(user);
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        @Nullable final UserDTO user = findOneByIndex(index);
        if (user == null) return;
        @NotNull final List<SessionDTO> sessions = entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId", SessionDTO.class)
                .setParameter("userId", user.getId())
                .getResultList();
        @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setParameter("userId", user.getId())
                .getResultList();
        @NotNull final List<ProjectDTO> projects = entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", user.getId())
                .getResultList();
        for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
        for (@NotNull final SessionDTO session : sessions) entityManager.remove(session);
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final List<TaskDTO> projectTasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                    .setParameter("projectId", project.getId())
                    .getResultList();
            for (@NotNull final TaskDTO projectTask : projectTasks) entityManager.remove(projectTask);
            entityManager.remove(project);
        }
        entityManager.remove(user);
    }

    @Override
    public void removeAll() {
        @NotNull final List<UserDTO> users = findAll();
        for (@NotNull final UserDTO user : users) {
            @NotNull final List<TaskDTO> tasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                    .setParameter("userId", user.getId())
                    .getResultList();
            @NotNull final List<SessionDTO> sessions = entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId", SessionDTO.class)
                    .setParameter("userId", user.getId())
                    .getResultList();
            @NotNull final List<ProjectDTO> projects = entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                    .setParameter("userId", user.getId())
                    .getResultList();
            for (@NotNull final SessionDTO session : sessions) entityManager.remove(session);
            for (@NotNull final TaskDTO task : tasks) entityManager.remove(task);
            for (@NotNull final ProjectDTO project : projects) {
                @NotNull final List<TaskDTO> projectTasks = entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId", TaskDTO.class)
                        .setParameter("projectId", project.getId())
                        .getResultList();
                for (@NotNull final TaskDTO projectTask : projectTasks) entityManager.remove(projectTask);
                entityManager.remove(project);
            }
            entityManager.remove(user);
        }
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        return entityManager.createQuery("SELECT u FROM UserDTO u WHERE u.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        return entityManager.createQuery("SELECT u FROM UserDTO u WHERE u.email = :email", UserDTO.class)
                .setParameter("email", email)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }
    
}

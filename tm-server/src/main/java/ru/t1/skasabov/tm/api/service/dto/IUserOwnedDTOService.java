package ru.t1.skasabov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IDTOService<M> {

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    long getSize(@Nullable String userId);

    @Nullable
    M removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

}

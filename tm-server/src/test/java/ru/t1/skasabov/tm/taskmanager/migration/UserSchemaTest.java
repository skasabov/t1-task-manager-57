package ru.t1.skasabov.tm.taskmanager.migration;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

public class UserSchemaTest extends AbstractSchemaTest {

    @Test
    @SneakyThrows
    public void test() {
        @NotNull final Liquibase liquibase = liquibase();
        liquibase.dropAll();
        liquibase.update("user");
    }

}

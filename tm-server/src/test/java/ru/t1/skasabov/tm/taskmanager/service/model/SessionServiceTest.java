package ru.t1.skasabov.tm.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.service.model.ISessionService;
import ru.t1.skasabov.tm.api.service.model.IUserService;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.field.IdEmptyException;
import ru.t1.skasabov.tm.exception.field.IndexIncorrectException;
import ru.t1.skasabov.tm.exception.field.UserIdEmptyException;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionServiceTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static User userOne;

    @NotNull
    private static User userTwo;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private IUserService userService;

    @NotNull
    private ISessionService sessionService;

    @Before
    public void initTest() {
        userService = context.getBean(IUserService.class);
        sessionService = context.getBean(ISessionService.class);
        userOne = userService.create("user_one", "user_one");
        userTwo = userService.create("user_two", "user_two");
        USER_ID_ONE = userOne.getId();
        USER_ID_TWO = userTwo.getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i <= 5) session.setUser(userOne);
            else session.setUser(userTwo);
            sessionService.add(session);
        }
    }

    @Test
    public void testAdd() {
        final long expectedNumberOfEntries = sessionService.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUser(userTwo);
        sessionService.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNull() {
        sessionService.add(null);
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = sessionService.getSize() + 4;
        @NotNull final List<Session> actualSessions = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final Session session = new Session();
            session.setUser(userOne);
            actualSessions.add(session);
        }
        sessionService.addAll(actualSessions);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testAddAllNull() {
        final long expectedNumberOfEntries = sessionService.getSize();
        sessionService.addAll(null);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Session> actualSessions = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            session.setUser(userTwo);
            actualSessions.add(session);
        }
        sessionService.set(actualSessions);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionService.getSize());
    }

    @Test
    public void testSetNull() {
        final long expectedNumberOfEntries = sessionService.getSize();
        sessionService.set(null);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testClearAll() {
        sessionService.removeAll();
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final long expectedNumberOfEntries = sessionService.getSize() - NUMBER_OF_ENTRIES / 2;
        sessionService.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = sessionService.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Session> sessionList = sessionService.findAll(USER_ID_TWO);
        sessionService.removeAll(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Session> sessionList = sessionService.findAll();
        Assert.assertEquals(sessionList.size(), sessionService.getSize());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Session> sessionList = sessionService.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionList.size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForEmptyUser() {
        sessionService.findAll("");
    }

    @Test
    public void testFindById() {
        @NotNull final Session session = sessionService.findAll().get(0);
        @NotNull final String sessionId = sessionService.findAll().get(0).getId();
        @Nullable final Session actualSession = sessionService.findOneById(sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUser().getId(), actualSession.getUser().getId());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyId() {
        Assert.assertNull(sessionService.findOneById(""));
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final Session session = sessionService.findAll(USER_ID_ONE).get(0);
        @NotNull final String sessionId = sessionService.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final Session actualSession = sessionService.findOneById(USER_ID_ONE, sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUser().getId(), actualSession.getUser().getId());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyIdForUser() {
        Assert.assertNull(sessionService.findOneById(USER_ID_ONE, ""));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIdForEmptyUser() {
        @NotNull final String sessionId = sessionService.findAll(USER_ID_ONE).get(0).getId();
        sessionService.findOneById("", sessionId);
    }

    @Test
    public void testFindByIdSessionNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionService.findOneById(id));
    }

    @Test
    public void testFindByIdSessionNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionService.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Session session = sessionService.findAll().get(0);
        @Nullable final Session actualSession = sessionService.findOneByIndex(0);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUser().getId(), actualSession.getUser().getId());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndex() {
        sessionService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndex() {
        sessionService.findOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndex() {
        sessionService.findOneByIndex((int) sessionService.getSize() + 1);
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Session session = sessionService.findAll(USER_ID_TWO).get(0);
        @Nullable final Session actualSession = sessionService.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUser().getId(), actualSession.getUser().getId());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndexForUser() {
        sessionService.findOneByIndex(USER_ID_TWO, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndexForUser() {
        sessionService.findOneByIndex(USER_ID_TWO, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndexForUser() {
        sessionService.findOneByIndex(USER_ID_TWO, 12);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindByIndexForEmptyUser() {
        sessionService.findOneByIndex("", 0);
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = sessionService.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUser(userOne);
        sessionService.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final Session session = new Session();
        session.setUser(userTwo);
        sessionService.add(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, sessionService.getSize(USER_ID_TWO));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForEmptyUser() {
        sessionService.getSize("");
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = sessionService.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionService.existsById(invalidId));
        Assert.assertTrue(sessionService.existsById(validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyId() {
        sessionService.existsById("");
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = sessionService.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionService.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(sessionService.existsById(USER_ID_ONE, validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyIdForUser() {
        sessionService.existsById(USER_ID_ONE, "");
    }

    @Test(expected = UserIdEmptyException.class)
    public void testIsNotFoundForEmptyUser() {
        sessionService.existsById("", sessionService.findAll(USER_ID_ONE).get(0).getId());
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = sessionService.getSize(USER_ID_ONE) - 1;
        @NotNull final Session session = sessionService.findAll(USER_ID_ONE).get(0);
        sessionService.removeOne(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(USER_ID_ONE));
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNull() {
        sessionService.removeOne(null);
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = sessionService.getSize() - 1;
        @NotNull final String sessionId = sessionService.findAll().get(0).getId();
        sessionService.removeOneById(sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final long expectedNumberOfEntries = sessionService.getSize(USER_ID_ONE) - 1;
        @NotNull final String sessionId = sessionService.findAll(USER_ID_ONE).get(0).getId();
        sessionService.removeOneById(USER_ID_ONE, sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(USER_ID_ONE));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyId() {
        Assert.assertNull(sessionService.removeOneById(""));
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyIdForUser() {
        sessionService.removeOneById(USER_ID_ONE, "");
    }

    @Test
    public void testRemoveByIdSessionNotFound() {
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertNull(sessionService.removeOneById(invalidId));
    }

    @Test
    public void testRemoveByIdSessionNotFoundForUser() {
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertNull(sessionService.removeOneById(USER_ID_ONE, invalidId));
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = sessionService.getSize() - 1;
        sessionService.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndex() {
        sessionService.removeOneByIndex(null);
    }

    @Test
    public void testRemoveByIndexForUser() {
        final long expectedNumberOfEntries = sessionService.getSize(USER_ID_TWO) - 1;
        sessionService.removeOneByIndex(USER_ID_TWO, 0);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(USER_ID_TWO));
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByEmptyIndexForUser() {
        sessionService.removeOneByIndex(USER_ID_TWO, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveByIndexForEmptyUser() {
        sessionService.removeOneByIndex("", 0);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndex() {
        sessionService.removeOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndexForUser() {
        sessionService.removeOneByIndex(USER_ID_TWO, -2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndex() {
        sessionService.removeOneByIndex((int) sessionService.getSize() + 1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndexForUser() {
        sessionService.removeOneByIndex(USER_ID_TWO, 5);
    }

    @After
    public void clearRepository() {
        userService.removeAll();
    }

}

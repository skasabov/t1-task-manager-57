package ru.t1.skasabov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.listener.EntityListener;

public interface IReceiverService {

    void receive(@NotNull EntityListener entityListener);

}

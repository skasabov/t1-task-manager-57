package ru.t1.skasabov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.api.IReceiverService;
import ru.t1.skasabov.tm.listener.EntityListener;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    public void init() {
        receiverService.receive(entityListener);
    }

}

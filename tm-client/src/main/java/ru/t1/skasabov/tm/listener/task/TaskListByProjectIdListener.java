package ru.t1.skasabov.tm.listener.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.TaskGetByProjectIdRequest;
import ru.t1.skasabov.tm.dto.response.TaskGetByProjectIdResponse;
import ru.t1.skasabov.tm.event.ConsoleEvent;
import ru.t1.skasabov.tm.util.TerminalUtil;

@Component
@NoArgsConstructor
public final class TaskListByProjectIdListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-show-by-project-id";

    @NotNull
    private static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskListByProjectIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest(projectId);
        request.setToken(getToken());
        @NotNull final TaskGetByProjectIdResponse response = taskEndpoint.findAllByProjectId(request);
        renderTasks(response.getTasks());
    }

}

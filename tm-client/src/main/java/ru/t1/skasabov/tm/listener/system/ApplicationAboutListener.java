package ru.t1.skasabov.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.skasabov.tm.dto.response.ApplicationAboutResponse;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
@NoArgsConstructor
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "about";

    @NotNull
    private static final String DESCRIPTION = "Show about program.";

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String argument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.command() == #consoleEvent.name " +
            "or @applicationAboutListener.argument() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = systemEndpoint.getAbout(request);
        System.out.println("name: " + response.getName());
        System.out.println("email: " + response.getEmail());
    }

}

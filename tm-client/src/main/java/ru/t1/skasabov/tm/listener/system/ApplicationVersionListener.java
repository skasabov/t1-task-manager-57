package ru.t1.skasabov.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.skasabov.tm.dto.response.ApplicationVersionResponse;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
@NoArgsConstructor
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "version";

    @NotNull
    private static final String DESCRIPTION = "Show program version.";

    @NotNull
    private static final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String argument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationVersionListener.command() == #consoleEvent.name " +
            "or @applicationVersionListener.argument() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull final ApplicationVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

}

package ru.t1.skasabov.tm.listener.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.TaskClearRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
@NoArgsConstructor
public final class TaskClearListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-clear";

    @NotNull
    private static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskClearListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASKS CLEAR]");
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        request.setToken(getToken());
        taskEndpoint.clearTasks(request);
    }

}

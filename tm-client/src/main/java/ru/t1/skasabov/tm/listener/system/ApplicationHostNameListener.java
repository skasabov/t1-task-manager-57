package ru.t1.skasabov.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ApplicationHostNameRequest;
import ru.t1.skasabov.tm.dto.response.ApplicationHostNameResponse;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
@NoArgsConstructor
public final class ApplicationHostNameListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "host";

    @NotNull
    private static final String DESCRIPTION = "Show host name.";

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationHostNameListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HOST]");
        @NotNull final ApplicationHostNameRequest request = new ApplicationHostNameRequest();
        @NotNull final ApplicationHostNameResponse response = systemEndpoint.getHostName(request);
        System.out.println(response.getHostName());
    }

}

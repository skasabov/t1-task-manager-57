package ru.t1.skasabov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.skasabov.tm.listener.AbstractListener;

@Component
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @Nullable
    @Override
    public String argument() {
        return null;
    }

}

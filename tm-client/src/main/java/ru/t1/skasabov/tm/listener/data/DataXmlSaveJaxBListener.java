package ru.t1.skasabov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.DataXmlSaveJaxBRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
public final class DataXmlSaveJaxBListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-xml-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Save data to xml file.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlSaveJaxBListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE XML]");
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest();
        request.setToken(getToken());
        domainEndpoint.saveDataXmlJaxB(request);
    }

}

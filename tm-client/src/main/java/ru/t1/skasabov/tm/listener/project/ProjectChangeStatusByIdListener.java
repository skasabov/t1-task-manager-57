package ru.t1.skasabov.tm.listener.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ProjectChangeStatusByIdRequest;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.event.ConsoleEvent;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
@NoArgsConstructor
public final class ProjectChangeStatusByIdListener extends AbstractProjectListener {

    @NotNull
    private static final String NAME = "project-change-status-by-id";

    @NotNull
    private static final String DESCRIPTION = "Change project status by id.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectChangeStatusByIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(id, status);
        request.setToken(getToken());
        projectEndpoint.changeProjectStatusById(request);
    }

}

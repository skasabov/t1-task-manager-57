package ru.t1.skasabov.tm.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.skasabov.tm.api.endpoint.IUserEndpoint;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.taskmanager.marker.SoapCategory;
import ru.t1.skasabov.tm.util.HashUtil;

import javax.xml.ws.WebServiceException;

public class UserEndpointTest extends AbstractEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = context.getBean(IAuthEndpoint.class);

    @NotNull
    private final IUserEndpoint userEndpoint = context.getBean(IUserEndpoint.class);

    @NotNull
    private final IPropertyService propertyService = context.getBean(IPropertyService.class);

    @Nullable
    private String userToken;

    @Nullable
    private String adminToken;

    private boolean isDeleteUserOne = true;

    private boolean isDeleteUserTwo = false;

    @Before
    public void initTest() {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(
                "123", "123", "123@123.ru"
        );
        userEndpoint.registryUser(request);
        @NotNull final UserRoleRegistryRequest adminRegistryRequest = new UserRoleRegistryRequest(
                "012", "012", Role.ADMIN
        );
        userEndpoint.registryUserRole(adminRegistryRequest);
        @NotNull final UserLoginRequest userRequest = new UserLoginRequest("123", "123");
        @NotNull final UserLoginResponse userResponse = authEndpoint.login(userRequest);
        userToken = userResponse.getToken();
        @NotNull final UserLoginRequest adminRequest = new UserLoginRequest("012", "012");
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(adminRequest);
        adminToken = adminResponse.getToken();
    }

    @Test
    @Category(SoapCategory.class)
    public void testLockUser() {
        @NotNull final UserLockRequest request = new UserLockRequest("123");
        request.setToken(adminToken);
        @NotNull final UserLockResponse response = userEndpoint.lockUser(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals(true, response.getUser().getLocked());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testLockUserEmptyLogin() {
        @NotNull final UserLockRequest request = new UserLockRequest("");
        request.setToken(adminToken);
        userEndpoint.lockUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testLockUserIncorrectLogin() {
        @NotNull final UserLockRequest request = new UserLockRequest("789");
        request.setToken(adminToken);
        userEndpoint.lockUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testLockUserNoAuth() {
        @NotNull final UserLockRequest request = new UserLockRequest("123");
        userEndpoint.lockUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testLockUserNoAdmin() {
        @NotNull final UserLockRequest request = new UserLockRequest("123");
        request.setToken(userToken);
        userEndpoint.lockUser(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testUnlockUser() {
        @NotNull final UserUnlockRequest request = new UserUnlockRequest("123");
        request.setToken(adminToken);
        @NotNull final UserUnlockResponse response = userEndpoint.unlockUser(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals(false, response.getUser().getLocked());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUnlockUserEmptyLogin() {
        @NotNull final UserUnlockRequest request = new UserUnlockRequest("");
        request.setToken(adminToken);
        userEndpoint.unlockUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUnlockUserIncorrectLogin() {
        @NotNull final UserUnlockRequest request = new UserUnlockRequest("789");
        request.setToken(adminToken);
        userEndpoint.unlockUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUnlockUserNoAuth() {
        @NotNull final UserUnlockRequest request = new UserUnlockRequest("123");
        userEndpoint.unlockUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUnlockUserNoAdmin() {
        @NotNull final UserUnlockRequest request = new UserUnlockRequest("123");
        request.setToken(userToken);
        userEndpoint.unlockUser(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRemoveUser() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest("123");
        request.setToken(adminToken);
        @NotNull final UserRemoveResponse response = userEndpoint.removeUser(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals("123", response.getUser().getLogin());
        Assert.assertEquals("123@123.ru", response.getUser().getEmail());
        isDeleteUserOne = false;
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveUserEmptyLogin() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest("");
        request.setToken(adminToken);
        userEndpoint.removeUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveUserIncorrectLogin() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest("789");
        request.setToken(adminToken);
        userEndpoint.removeUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveUserNoAuth() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest("123");
        userEndpoint.removeUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRemoveUserNoAdmin() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest("123");
        request.setToken(userToken);
        userEndpoint.removeUser(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testChangePassword() {
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest("1234");
        request.setToken(userToken);
        @NotNull final UserChangePasswordResponse response = userEndpoint.changeUserPassword(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals(HashUtil.salt(propertyService, "1234"), response.getUser().getPasswordHash());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangeEmptyPassword() {
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest("");
        request.setToken(userToken);
        userEndpoint.changeUserPassword(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testChangePasswordNoAuth() {
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest("1234");
        userEndpoint.changeUserPassword(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testUpdateProfile() {
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(
                "123", "123", "123"
        );
        request.setToken(userToken);
        @NotNull final UserUpdateProfileResponse response = userEndpoint.updateUserProfile(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals("123", response.getUser().getLastName());
        Assert.assertEquals("123", response.getUser().getFirstName());
        Assert.assertEquals("123", response.getUser().getMiddleName());
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testUpdateProfileNoAuth() {
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(
                "123", "123", "123"
        );
        userEndpoint.updateUserProfile(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRegistryUser() {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(
                "456", "456", "456@456.ru"
        );
        @NotNull final UserRegistryResponse response = userEndpoint.registryUser(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals("456", response.getUser().getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "456"), response.getUser().getPasswordHash());
        Assert.assertEquals("456@456.ru", response.getUser().getEmail());
        isDeleteUserTwo = true;
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRegistryUserEmptyLogin() {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(
                "", "456", "456@456.ru"
        );
        userEndpoint.registryUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRegistryUserEmptyPassword() {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(
                "456", "", "456@456.ru"
        );
        userEndpoint.registryUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRegistryUserEmptyEmail() {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(
                "456", "456", ""
        );
        userEndpoint.registryUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRegistryUserRepeatLogin() {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(
                "123", "456", "456@456.ru"
        );
        userEndpoint.registryUser(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRegistryUserRepeatEmail() {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(
                "456", "456", "123@123.ru"
        );
        userEndpoint.registryUser(request);
    }

    @Test
    @Category(SoapCategory.class)
    public void testRegistryUserRole() {
        @NotNull final UserRoleRegistryRequest request = new UserRoleRegistryRequest(
                "456", "456", Role.ADMIN
        );
        @NotNull final UserRoleRegistryResponse response = userEndpoint.registryUserRole(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals("456", response.getUser().getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "456"), response.getUser().getPasswordHash());
        Assert.assertEquals(Role.ADMIN, response.getUser().getRole());
        isDeleteUserTwo = true;
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRegistryUserRoleEmptyLogin() {
        @NotNull final UserRoleRegistryRequest request = new UserRoleRegistryRequest(
                "", "456", Role.ADMIN
        );
        userEndpoint.registryUserRole(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRegistryUserRoleEmptyPassword() {
        @NotNull final UserRoleRegistryRequest request = new UserRoleRegistryRequest(
                "456", "", Role.ADMIN
        );
        userEndpoint.registryUserRole(request);
    }

    @Test(expected = WebServiceException.class)
    @Category(SoapCategory.class)
    public void testRegistryUserRoleRepeatLogin() {
        @NotNull final UserRoleRegistryRequest request = new UserRoleRegistryRequest(
                "123", "456", Role.ADMIN
        );
        userEndpoint.registryUserRole(request);
    }

    @After
    public void endTest() {
        @NotNull final UserLogoutRequest userRequest = new UserLogoutRequest();
        userRequest.setToken(userToken);
        authEndpoint.logout(userRequest);
        if (isDeleteUserOne) {
            @NotNull final UserRemoveRequest requestOne = new UserRemoveRequest("123");
            requestOne.setToken(adminToken);
            userEndpoint.removeUser(requestOne);
        }
        if (isDeleteUserTwo) {
            @NotNull final UserRemoveRequest requestTwo = new UserRemoveRequest("456");
            requestTwo.setToken(adminToken);
            userEndpoint.removeUser(requestTwo);
        }
        @NotNull final UserLogoutRequest adminRequest = new UserLogoutRequest();
        adminRequest.setToken(adminToken);
        authEndpoint.logout(adminRequest);
        @NotNull final UserRemoveRequest adminRemoveRequest = new UserRemoveRequest("012");
        adminRemoveRequest.setToken(adminToken);
        userEndpoint.removeUser(adminRemoveRequest);
    }

}

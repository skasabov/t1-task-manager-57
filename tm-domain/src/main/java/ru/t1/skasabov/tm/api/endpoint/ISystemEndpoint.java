package ru.t1.skasabov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull String NAME = "SystemEndpoint";

    @NotNull String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ApplicationAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationAboutRequest request
    );

    @NotNull
    @WebMethod
    ApplicationVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationVersionRequest request
    );

    @NotNull
    @WebMethod
    ApplicationSystemInfoResponse getSystemInfo(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationSystemInfoRequest request
    );

    @NotNull
    ApplicationHostNameResponse getHostName(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationHostNameRequest request
    );

    @NotNull
    @WebMethod
    ApplicationUpdateSchemaResponse updateSchema(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationUpdateSchemaRequest request
    );

    @NotNull
    @WebMethod
    ApplicationDropSchemaResponse dropSchema(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ApplicationDropSchemaRequest request
    );

}

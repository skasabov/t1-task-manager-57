package ru.t1.skasabov.tm.exception.system;

public final class ArgumentEmptyException extends AbstractSystemException {

    public ArgumentEmptyException() {
        super("Error! Argument is empty...");
    }

}

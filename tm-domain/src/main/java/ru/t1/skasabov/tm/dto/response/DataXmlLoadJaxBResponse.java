package ru.t1.skasabov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DataXmlLoadJaxBResponse extends AbstractResponse {
}
